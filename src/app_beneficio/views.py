from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.contrib.auth.views import LoginView

from .models import Beneficio
from .forms import BeneficioForm

# Create your views here.
class BeneficiosListView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    model = Beneficio
    #queryset = Beneficio.objects.filter(vigente=True)
    permission_required = 'app_beneficio.view_beneficio'
    template_name = 'beneficios/lista.html'

class BeneficiosListViewActivos(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    #model = Beneficio
    queryset = Beneficio.objects.filter(vigente=True)
    permission_required = [ 'app_beneficio.view_beneficio',]
    template_name = 'beneficios/lista.html'

class BeneficiosListViewInactivos(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    #model = Beneficio
    queryset = Beneficio.objects.filter(vigente=False)
    permission_required = [ 'app_beneficio.view_beneficio',]
    template_name = 'beneficios/lista.html'

class BeneficiosDetailView(LoginRequiredMixin, DetailView):
    model = Beneficio
    template_name = 'beneficios/detalle.html'

@login_required
def alta_beneficio(request):

    if request.method == 'POST':

        form = BeneficioForm(data=request.POST)

        if form.is_valid():
            form.save()

    
    form = BeneficioForm()

    return render(request, 'beneficios/alta.html', {
        'form': form
    })


@login_required
def sacar_vigencia(request, pk):

    beneficio = Beneficio.objects.get(pk=pk)
    beneficio.vigente = False
    beneficio.save()

    return redirect('')




