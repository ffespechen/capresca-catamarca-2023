from django import forms
from .models import Beneficio

# class BeneficioForm(forms.Form):
#     nombre = forms.CharField(max_length=50)
#     descripcion = forms.CharField(max_length=150)
#     vigente = forms.BooleanField()

class BeneficioForm(forms.ModelForm):
    class Meta:
        model = Beneficio
        fields = [ 'nombre', 'descripcion', 'vigente']