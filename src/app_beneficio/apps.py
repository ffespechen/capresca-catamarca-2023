from django.apps import AppConfig


class AppBeneficioConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "app_beneficio"
