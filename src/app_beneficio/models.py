from django.db import models

# Create your models here.
class Beneficio(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    vigente = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return F"{self.nombre} - Creado el {self.creado} Vigente: { self.vigente }"

    class Meta:
        ordering = ('-modificado', )


