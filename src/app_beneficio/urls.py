from django.urls import path 
from .views import BeneficiosListView, BeneficiosDetailView, BeneficiosListViewActivos, alta_beneficio, sacar_vigencia

app_name = 'beneficios'

urlpatterns = [
    path('', BeneficiosListView.as_view(), name='lista-beneficio'),
    path('activos/', BeneficiosListViewActivos.as_view(), name='lista-activos'),
    path('<int:pk>/', BeneficiosDetailView.as_view(), name='detalle-beneficio'),
    path('alta/', alta_beneficio, name='alta-beneficio'),
    path('baja/<int:pk>', sacar_vigencia, name='baja-beneficio')
]
