from django.urls import path 
from .views import listar_beneficiarios, detalle_beneficiario, crear_beneficiario

app_name = 'beneficiarios'

urlpatterns = [
    path('', listar_beneficiarios, name='listar-beneficiarios'),
    path('<int:pk>/', detalle_beneficiario, name='detalle-benefiario'),
    path('alta/', crear_beneficiario, name='crear-beneficiario')
]
