from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from .models import Beneficiario 
from .forms import  BeneficiarioForm 

# Create your views here.
# Vistas basadas en funciones
@login_required
@permission_required('app_beneficiario.view_beneficiario')
def listar_beneficiarios(request):

    beneficiarios = Beneficiario.objects.all()

    return render(request, 'beneficiarios/lista.html', {
        'titulo': 'Listado de beneficiarios',
        'beneficiarios': beneficiarios, 
    })

@login_required
@permission_required('app_beneficiario.view_beneficiario')
def detalle_beneficiario(request, pk):

    beneficiario = Beneficiario.objects.get(pk=pk)

    return render(request, 'beneficiarios/detalle.html', {
        'titulo': 'Detalle del beneficiario',
        'beneficiario': beneficiario
    })

@login_required
@permission_required('app_beneficiario.add_beneficiario')
def crear_beneficiario(request):

    if request.method == 'POST':
        form = BeneficiarioForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
        else:
            print(form.errors)

    form = BeneficiarioForm()

    return render(request, 'beneficiarios/alta.html', {
        'titulo': 'Alta de beneficiarios',
        'form': form
    })




