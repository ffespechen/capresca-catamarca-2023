from django.db import models 
from django.contrib.auth.models import User 

from app_beneficio.models import Beneficio 

# Create your models here.
class Beneficiario(models.Model):
    GENEROS = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('X', 'No Binario'),
        ('N', 'No Informado')
    )

    beneficio = models.ForeignKey(Beneficio, on_delete=models.CASCADE)
    creado_por = models.ForeignKey(User, on_delete=models.CASCADE)
    apellido = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    dni = models.PositiveIntegerField(unique=True)
    cuil = models.PositiveBigIntegerField(unique=True)
    genero = models.CharField(max_length=1, choices=GENEROS, default='N')
    imagen = models.ImageField(upload_to='beneficiarios_fotos', default='usuario.png')
    domicilio = models.CharField(max_length=100)
    alta_beneficio = models.DateTimeField()
    baja_beneficio = models.DateTimeField(null=True, blank=True)
    activo = models.BooleanField(default=False)
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return F"{self.apellido.upper()}, {self.nombre.title()} cuil {self.cuil} activo: {self.activo}"
    

    class Meta:
        ordering = ('apellido', 'nombre', 'dni')