from django import forms 
from .models import Beneficiario 


class BeneficiarioForm(forms.ModelForm):
    class Meta:
        model = Beneficiario
        fields = '__all__'
        widgets = {
            'alta_beneficio': forms.SelectDateWidget(),
            'baja_beneficio': forms.SelectDateWidget()
        }

        